package org.eyo.toposervice;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TopoApiTest {

    @LocalServerPort
    int port;
    @Autowired
    private TopographicDetailsCrudRepository repository;

    @BeforeEach
    void setUp() {
        RestAssured.port = port;
    }

    @Test
    void whenGetMadrid_shouldGetFlatLandscape(){
        given()
                .request()
                .get("/api/topographicdetails/Madrid")
                .then()
                .statusCode(200)
                .body("landscape", is("flat"));
    }

    @Test
    void givenHawaiCreated_whenGetHawai_shouldReturnFlatInLandscape() {
        given()
                .request()
                .body("{ \"id\" : \"Hawai\", \"landscape\": \"flat\" }")
                .contentType(ContentType.JSON)
                .post("/api/topographicdetails")
                .then()
                .statusCode(201);


        given()
                .request()
                .get("/api/topographicdetails/Hawai")
                .then()
                .statusCode(200)
                .body("landscape", is("flat"));
    }
}
