package org.eyo.toposervice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class TopographicDetailsTest {

    private TopographicDetails topographicDetails;

    @BeforeEach
    void setUp(){
        this.topographicDetails = new TopographicDetails();
    }

    @Test
    void should_not_be_null(){
        assertNotNull(this.topographicDetails);
    }
}
