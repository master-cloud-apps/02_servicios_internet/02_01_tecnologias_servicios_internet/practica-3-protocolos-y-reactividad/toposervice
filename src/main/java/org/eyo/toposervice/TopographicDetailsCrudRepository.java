package org.eyo.toposervice;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface TopographicDetailsCrudRepository extends ReactiveCrudRepository<TopographicDetails, String> {

    Flux<TopographicDetails> findAllByLandscape(String landscape);
    Mono<TopographicDetails> findFirstByCity(String city);
}
