package org.eyo.toposervice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Random;

@RestController
@RequestMapping("/api/topographicdetails")
public class TopographicDetailsController {

    private TopographicDetailsCrudRepository repository;

    public TopographicDetailsController(TopographicDetailsCrudRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/{city}")
    public Mono<CityDTO> getTopographicDetails(@PathVariable String city) {
        return repository.findFirstByCity(city)
                .delayElement(Duration.ofSeconds(new Random().nextInt(3) + 1L))
                .map(this::toCityDTO);
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<CityDTO> createCityLandscape(@RequestBody Mono<CityDTO> city) {
        return city
                .map(this::toTopo)
                .flatMap(repository::save)
                .map(this::toCityDTO);
    }

    private TopographicDetails toTopo(CityDTO cityDTO) {
        return new TopographicDetails(null, cityDTO.getId(), cityDTO.getLandscape());
    }

    private CityDTO toCityDTO(TopographicDetails topo){
        return new CityDTO(topo.getCity(), topo.getLandscape());
    }
}
