package org.eyo.toposervice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Document
@AllArgsConstructor
@NoArgsConstructor
public class TopographicDetails {

    @Id
    private String id;
    @Indexed(unique = true)
    private String city;
    private String landscape;
}
