# TopoService

Devuelve información sobre la orografía del terreno.

## Detalles de implementación

* Ofrecerá una API REST en la que recibirá una ciudad como parámetro y devolverá “Flat” o “Mountain”.
* Consulta
    * URL: /api/topographicdetails/Madrid
    * Method: GET
    * Response Body: 
        ````json
        {
          "id": "Madrid",
          "landscape":"flat" 
        }
        ````
* Se implementará en Java con SpringBoot.
* Tendrá una lista de ciudades asociadas a su landscape `Flat` o `Mountain` guardadas en una MongoDB.
* Se implementará de forma reactiva funcional con WebFlux.
* Simulará un tiempo de proceso de 1 a 3 segundos aleatorio.